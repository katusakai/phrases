#!/bin/bash

if ! [ -x "$(command -v docker-compose)" ]; then
  echo '### Error: docker-compose is not installed.' >&2
  exit 1
fi

echo '### Running composer install...'
docker-compose -f docker-compose-helpers.yml run --rm composer install

echo '### Running npm install...'
docker-compose -f docker-compose-helpers.yml run --rm node npm install

echo "### Preparing backend containers for data seeding"
docker-compose up -d --build api php db

if [ "$1" != 'prod' ]; then
  echo "### Preparing remaining containers for development"
  docker-compose build --no-cache frontend_dev

else
  echo "### Preparing remaining containers for production"
  docker-compose build --no-cache frontend_prod

fi

echo "### Seeding default database data"
if [ "$OS" = "Windows_NT" ]; then
  winpty docker-compose exec php php bin/console d:m:m
  winpty docker-compose exec php php bin/console d:fixtures:load

else
  docker-compose exec php php bin/console d:m:m
  docker-compose exec php php bin/console d:fixtures:load

fi

echo "### Turning down containers"
docker-compose down

