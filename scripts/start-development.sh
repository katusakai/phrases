#!/bin/bash

WEB_DEV_PORT=$(grep WEB_DEV_PORT .env | cut -d '=' -f2)
API_PORT=$(grep API_PORT .env | cut -d '=' -f2)

echo '### Starting services for development'
docker-compose up -d frontend_dev api php db

echo '###Waiting for development environment to start...'
sleep 30
echo '### Services for production are ready'
echo "### Your development app is reachable by http://localhost:$WEB_DEV_PORT"
echo "### Your api is reachable by http://localhost:$API_PORT"
