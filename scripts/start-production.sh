#!/bin/bash

WEB_PROD_PORT=$(grep WEB_PROD_PORT .env | cut -d '=' -f2)
API_PORT=$(grep API_PORT .env | cut -d '=' -f2)

echo '### Starting services for production'
docker-compose up -d frontend_prod api php db

echo '### Services for production are ready'
echo "### Your production app is reachable by http://localhost:$WEB_PROD_PORT"
echo "### Your api is reachable by http://localhost:$API_PORT"
