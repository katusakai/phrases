<?php

namespace App\DataFixtures;

use App\Entity\Phrase;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $phrasesJson = file_get_contents(__DIR__ . '/phrases.json');
        $phrases = json_decode($phrasesJson);

        foreach ($phrases as $dummyPhrase) {
            $phrase = new Phrase();
            $phrase->setFullPhrase($dummyPhrase->fullPhrase);
            $manager->persist($phrase);
            $manager->flush();
        }
    }
}
