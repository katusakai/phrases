<?php

namespace App\Entity;

use App\Repository\PhraseRepository;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\UuidInterface;
use Ramsey\Uuid\Doctrine\UuidGenerator;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=PhraseRepository::class)
 */
class Phrase implements \JsonSerializable
{
    /**
     * @var UuidInterface
     *
     * @ORM\Id
     * @ORM\Column(type="uuid", unique=true)
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class=UuidGenerator::class)
     */
    protected $id;

    public function getId()
    {
        return $this->id;
    }

    /**
     * @ORM\Column(type="text")
     * @Assert\NotBlank
     */
    protected $fullPhrase;

    public function getFullPhrase(): ?string
    {
        return $this->fullPhrase;
    }

    public function setFullPhrase(string $fullPhrase): self
    {
        $this->fullPhrase = $fullPhrase;

        return $this;
    }

    public function jsonSerialize()
    {
        return [
            "id" => $this->getId(),
            "fullPhrase" => $this->getFullPhrase()
        ];
    }
}
