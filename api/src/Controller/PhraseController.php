<?php

namespace App\Controller;

use App\Entity\Phrase;
use App\Repository\PhraseRepository;
use Doctrine\DBAL\Types\ConversionException;
use Doctrine\ORM\EntityManagerInterface;
use App\Response\Messages;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Class PostController
 * @package App\Controller
 * @Route("/api/phrase", name="api_phrase_")
 */
class PhraseController extends ApiController
{
    protected $phraseRepository;

    protected $validator;

    protected $messages;

    /**
     * PhraseController constructor.
     * @param PhraseRepository $phraseRepository
     * @param Messages $messages
     * @param ValidatorInterface $validator
     */
    public function __construct(PhraseRepository $phraseRepository,
                                Messages $messages,
                                ValidatorInterface $validator)
    {
        $this->phraseRepository = $phraseRepository;
        $this->validator = $validator;
        $this->messages = $messages;
    }


    /**
     * @Route("", name="random", methods={"GET"})
     * @return JsonResponse
     */
    public function random(): JsonResponse
    {
        $phrase = $this->phraseRepository->findRandom();

        if ($phrase) {
            return $this->response($phrase);
        } else {

            $this->messages->set('Phrase not found');
            return $this->response($this->messages->get(), 404);
        }
    }

    /**
     * @Route("/{id}", name="show", methods={"GET"})
     * @param string $id
     * @return JsonResponse
     */
    public function show(string $id): JsonResponse
    {
        try {
            $phrase = $this->phraseRepository->find($id);
            if ($phrase) {
                return $this->response($phrase->jsonSerialize());
            } else {
                $this->messages->set('Phrase not found');
                return $this->response($this->messages->get(), 404);
            }
        }

        catch (ConversionException $e) {
            $this->messages->set('Phrase not found');
            return $this->response($this->messages->get(), 404);
        }
    }

    /**
     * @Route("", name="store", methods={"POST"})
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @return JsonResponse
     */
    public function store(Request $request,
                          EntityManagerInterface $entityManager): JsonResponse {

        if ($request->get(('fullPhrase')) === null) {
            $this->messages->set('Missing field `fullPhrase`');
            return $this->response($this->messages->get(), 403);
        }

        $phrase = new Phrase();
        $phrase->setFullPhrase($request->get('fullPhrase'));

        $errors = $this->validator->validate($phrase);

        if (count($errors) > 0) {
            $this->messages->fill($errors);
            return $this->response($this->messages->get(), 403);
        } else {

            $entityManager->persist($phrase);
            $entityManager->flush();
            return $this->response($phrase, 201);
        }
    }
}
