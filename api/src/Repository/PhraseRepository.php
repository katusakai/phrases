<?php

namespace App\Repository;

use App\Entity\Phrase;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Phrase|null find($id, $lockMode = null, $lockVersion = null)
 * @method Phrase|null findOneBy(array $criteria, array $orderBy = null)
 * @method Phrase[]    findAll()
 * @method Phrase[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PhraseRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Phrase::class);
    }

    public function findRandom()
    {
        $conn = $this->getEntityManager()->getConnection();
        $sql = 'SELECT * FROM phrase p
                ORDER BY RAND()
                LIMIT 1';
        $stmt = $conn->prepare($sql);
        $stmt->execute();

        $phrase = $stmt->fetch();

        if ($phrase) {
            return ['id' => $phrase['id'],
                'fullPhrase' => $phrase['full_phrase']];
        } else {
            return $phrase;
        }
    }

}
