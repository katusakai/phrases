<?php

namespace App\Response;


class Messages
{
    protected $messages;

    public function get()
    {
        return $this->messages;
    }

    public function fill($errors) {
        foreach ($errors as $error) {
            $this->messages['messages'][] = [$error->getpropertyPath() => $error->getMessage()];
        }
    }

    public function set($text)
    {
        $this->messages['message'] = $text;
    }
}
