<?php

namespace App\Tests\Controller;

use App\Entity\Phrase;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class PhraseControllerTest extends WebTestCase
{

    protected $client;

    protected $em;

    protected $repository;

    protected function setUp()
    {
        $this->client = $this->createClient(['environment' => 'test']);
        $this->client->disableReboot();
        $this->em = $this->client->getContainer()->get('doctrine.orm.entity_manager');
        $this->em->beginTransaction();
        $this->repository = $this->em->getRepository(Phrase::class);
    }

    public function testRandom()
    {
        $this->client->request(
            'GET',
            $this->client->getContainer()->get('router')->generate('api_phrase_random')
        );

        $response = json_decode($this->client->getResponse()->getContent());

        if ($this->client->getResponse()->getStatusCode() === 200) {
            $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
            $this->assertIsString($response->id);
            $this->assertNotEmpty($response->id);
            $this->assertIsString($response->fullPhrase);
            $this->assertNotEmpty($response->fullPhrase);

        } elseif ($this->client->getResponse()->getStatusCode() === 404) {
            $this->assertEquals(404, $this->client->getResponse()->getStatusCode());
            $this->assertIsString($response->message);
            $this->assertNotEmpty($response->message);
            $this->assertEquals('Phrase not found', $response->message);
        }

    }

    public function testShowOk()
    {
        $existingPhrase = $this->repository->findRandom();

        if ($existingPhrase) {
            $id = $existingPhrase['id'];
            $parameters = ['id' => $id];
            $this->client->request(
                'GET',
                $this->client->getContainer()->get('router')->generate('api_phrase_show', $parameters)
            );

            $statusCode = $this->client->getResponse()->getStatusCode();
            $this->assertEquals(200, $statusCode);
            $response = json_decode($this->client->getResponse()->getContent());
            $this->assertIsString($response->id);
            $this->assertNotEmpty($response->id);
            $this->assertIsString($response->fullPhrase);
            $this->assertNotEmpty($response->fullPhrase);

        } else {
            $this->assertFalse($existingPhrase);
        }

    }

    public function testShowNotFound()
    {
        $parameters = ['id' => 'wrong-id'];
        $this->client->request(
            'GET',
            $this->client->getContainer()->get('router')->generate('api_phrase_show', $parameters)
        );

        $this->assertEquals(404, $this->client->getResponse()->getStatusCode());
        $response = json_decode($this->client->getResponse()->getContent());
        $this->assertIsString($response->message);
        $this->assertNotEmpty($response->message);
        $this->assertEquals('Phrase not found', $response->message);
    }

    public function testStoreCreated()
    {
        $parameters = ['fullPhrase' => 'Random phrase to store'];
        $this->client->request(
            'POST',
            $this->client->getContainer()->get('router')->generate('api_phrase_store'),
            $parameters
        );

        $this->assertEquals(201, $this->client->getResponse()->getStatusCode());
        $response = json_decode($this->client->getResponse()->getContent());
        $this->assertIsString( $response->id);
        $this->assertNotEmpty($response->id);
        $this->assertIsString($response->fullPhrase);
        $this->assertNotEmpty($response->fullPhrase);
        $this->assertEquals($parameters['fullPhrase'], $response->fullPhrase);
    }

    public function testStoreForbiddenEmpty()
    {
        $parameters = ['fullPhrase' => ''];
        $this->client->request(
            'POST',
            $this->client->getContainer()->get('router')->generate('api_phrase_store'),
            $parameters
        );

        $this->assertEquals(403, $this->client->getResponse()->getStatusCode());
        $response = json_decode($this->client->getResponse()->getContent());

        $this->assertIsArray($response->messages);
        $this->assertNotEmpty($response->messages[0]->fullPhrase);
        $this->assertIsString($response->messages[0]->fullPhrase);
        $this->assertEquals('This value should not be blank.', $response->messages[0]->fullPhrase);
    }

    public function testStoreForbiddenNoParameters()
    {
        $this->client->request(
            'POST',
            $this->client->getContainer()->get('router')->generate('api_phrase_store')
        );

        $this->assertEquals(403, $this->client->getResponse()->getStatusCode());
        $response = json_decode($this->client->getResponse()->getContent());
        $this->assertIsString($response->message);
        $this->assertNotEmpty($response->message);
        $this->assertEquals('Missing field `fullPhrase`', $response->message);
    }

    protected function tearDown(): void
    {
        parent::tearDown();

        if($this->em->getConnection()->isTransactionActive()) {
            $this->em->rollback();
        }
    }
}
