import * as env from 'src/environments/env';
export const environment = {
  production:          true,
  appName:             'Phrases',
  backendUri:          '/api'
};
