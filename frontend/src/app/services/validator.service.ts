import { Injectable } from '@angular/core';
import { Validators } from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class ValidatorService {

  public fullPhrase;

  constructor() {
    this.fullPhrase = ['', [Validators.required]];
  }
}
