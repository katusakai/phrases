import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { environment } from "../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class PhraseService {

  private readonly prefix = '/phrase'

  constructor(
    private _http: HttpClient,
  ) { }

  public random() {
    return this._http.get(`${environment.backendUri}${this.prefix}`)
  }

  public show(id: string) {
    return this._http.get(`${environment.backendUri}${this.prefix}/${id}`)
  }

  public store(data: FormData) {
    return this._http.post(`${environment.backendUri}${this.prefix}`, data)
  }
}
