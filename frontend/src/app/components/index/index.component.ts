import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.scss']
})
export class IndexComponent implements OnInit {

  id: string | undefined;
  constructor(
    private _actRoute: ActivatedRoute
  ) {
  }

  ngOnInit(): void {
    this.id = this._actRoute.snapshot.params.id;
  }

}
