import { Component, Input, OnInit } from '@angular/core';
import { Phrase } from "../../../models/phrase";
import { PhraseService } from "../../services/phrase.service";

@Component({
  selector: 'app-phrase',
  templateUrl: './phrase.component.html',
  styleUrls: ['./phrase.component.scss']
})
export class PhraseComponent implements OnInit {

  @Input() id: string | undefined;

  phrase: Phrase;

  error: string;

  constructor(
    private _phrase: PhraseService
  ) {
  }

  ngOnInit(): void {
    this.initialize();
  }

  private initialize() {

    if (this.id) {
      this.initializeShow();
    } else {
      this.initializeRandom();
    }
  }

  private initializeShow() {

    this._phrase.show(this.id).subscribe((
      (response: any) => {
        this.phrase = response;
      }),
    error => {
        this.error = error.error.message;
      }
    )
  }

  private initializeRandom() {
    this._phrase.random().subscribe((
      (response: any) => {
        this.phrase = response;
      }),
    error => {
        this.error = error.error.message;
      }
    )
  }
}
