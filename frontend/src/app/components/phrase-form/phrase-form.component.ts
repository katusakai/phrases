import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from "@angular/forms";
import { ValidatorService } from "../../services/validator.service";
import {PhraseService} from "../../services/phrase.service";
import {Phrase} from "../../../models/phrase";

@Component({
  selector: 'app-phrase-form',
  templateUrl: './phrase-form.component.html',
  styleUrls: ['./phrase-form.component.scss']
})
export class PhraseFormComponent implements OnInit {

  public form: FormGroup;

  public errors: string[] = [];

  public message: string | null;

  public phrase: Phrase;

  constructor(
    private _formBuilder: FormBuilder,
    private Validator: ValidatorService,
    private _phrase: PhraseService
  ) { }

  ngOnInit(): void {
    this.form = this._formBuilder.group({
      fullPhrase: this.Validator.fullPhrase
    });
  }

  get f() { return this.form.controls; }

  protected resetValues(): void {
    this.errors = [];
    this.message = null;
    this.phrase = null;
  }

  public onCreate(): void {
    this.resetValues();
    const formData = new FormData();
    formData.append('fullPhrase', this.f.fullPhrase.value)

    this._phrase.store(formData).subscribe(
      (response: Phrase) => {
        this.message = 'Phrase created. Would you like to go to its page?'
        this.phrase = response
      },
      error => {
        error.error.messages?.forEach(message => {
          this.errors.push(message);
        })
      })
  }

}
