## Random Phrases

## Requirements
1. docker https://docs.docker.com/get-docker/
2. docker-compose https://docs.docker.com/compose/install/

## First time setup
1. Clone this repository.
2. Run `bash ./scripts/generate-env.sh` .
3. Edit .env file according to you. Make sure that you use empty ports.
4. If you plan to use it for development, make sure that API_PORT matches in .env frontend/src/environments/env.ts
5. Install:
    1. For development Run `bash ./scripts/install.sh`
    2. For production Run `bash ./scripts/install.sh prod`

## Usage

1. To start application:
    1. For development Run `bash ./scripts/start-development.sh`
    2. For production Run `bash ./scripts/start-production.sh`
2. To stop application Run `docker-compose down`
3. To use test Run `docker-compose exec php php bin/phpunit`

## Demo
https://phrases.tadasjanca.lt/


